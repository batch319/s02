const { factorial } = require('../src/util.js');
const { expect, assert } = require('chai');

it('test_fun_factorial_5!_is_120_revised', () => {
	const product = factorial(5);
	expect(product).to.equal(120);
});

it('test_fun_factorial_1!_is_1', () => {
	const product = factorial(1);
	expect(product).to.equal(1);
});

it('test_fun_factorial_1!_is_120_expect', () => {
	const product = factorial(1);
	expect(product).to.equal(1);
});

it('test_fun_factorial_-1!_is_undefined', () => {
	const product = factorial(-1);
	assert.equal(product, undefined);
});

