const { factorial, div_check } = require('../src/util.js');
const { expect, assert } = require('chai');

it('test_fun_factorial_5!_is_120', () => {
	const product = factorial(5);
	expect(product).to.equal(120);
});

it('test_fun_factorial_1!_is_1', () => {
	const product = factorial(1);
	expect(product).to.equal(1);
});

it('test_fun_factorial_0!_is_1', () => {
	const product = factorial(1);
	expect(product).to.equal(1);
});


it('test_fun_factorial_4!_is_24', () => {
	const product = factorial(1);
	expect(product).to.equal(1);
});

it('test_fun_factorial_10!_is_3628800', () => {
	const product = factorial(-1);
	assert.equal(product, undefined);
});

it('test_100_is_divisible_by_5', () => {
	const product = div_check();
	assert.equal(product, undefined);
});

it('test_49_is_divisible_by_7', () => {
	const product = div_check();
	assert.equal(product, undefined);
});

it('test_30_is_divisible_by_5', () => {
	const product = div_check();
	assert.equal(product, undefined);
});

it('test_56_is_divisible_by_7', () => {
	const product = div_check();
	assert.equal(product, undefined);
});